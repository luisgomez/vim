# Luis' Vim Configuration

Just a place to store my vim configuration, plugins, documentation, etc.

## Installation:

### Linux
1. Clone to a folder named `.vim` in your home folder
2. Edit the default vimrc (`.vimrc` in linux) to contain only the following line:

        source ~/.vim/vimrc

### Windows
1. Clone inside the pre-existing folder `vimfiles` in home folder (e.g. `C:\Users\coolguy\vimfiles`)
2. Modify the top-level `_vimrc` file inside your home folder (e.g. `C:\Users\coolguy\_vimrc`)

        source ~/vimfiles/vimrc

## Vimrc Reference

### Editing Shortcuts:
- Surround visual selection with `()`, `{}`, `[]`, `''`, `""`, or `<space>` by using `,(`, `,{`, `,[`, `,'`, `,"`, and `,<space>`
- Move current line, or visual line selection, up or down by using `<Ctrl>k` and `<Ctrl>j`

### Search ###
- Normal Mode: Search for word under the cursor with `,*`
- Find/replace in files using `Qargs`:

        :grep foobar | copen " lists all matching items in quickfix window
        :Qargs | argdo %s/foobar/newfoo/gce | update " replaces foobar with newfoo in every open file

### Source Code Indexing ###

- Command `Index` recursively indexes source code from current working directory
- Special file `.ctagsignore`: list of files/folders to be ignored (one per line)

