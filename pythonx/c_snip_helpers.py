#
# Snippet helpers
#

import re

def include_guard(snip_c, snip_fn):
    '''
    Generate a C include guard identifier.
    Transforms the current file name to upper case and underscore-delimited, and
    appends a 'H_'.

    Examples:
    --------
    my-file.h --> MY_FILE_H_
    SomeFile.h --> SOME_FILE_H_
    '''
    if snip_c == "":
        name = re.sub(r'(?!^)([^_-])([A-Z]+)', r'\1_\2', snip_fn).upper()+'_'
        name = re.sub(r'[.\-]', '_', name)
        return name
    else:
        return snip_c


