"
" A nice solution to search-and-replace-in-files adapted from:
"   http://stackoverflow.com/questions/5686206/search-replace-using-quickfix-list-in-vim
"

" Function "Qargs":
" adds every file in the quickfix list to the argument list
"
" Allows doing something like:
" :grep foobar | copen
" :Qargs | argdo %s/foobar/newfoo/gce | update
"
command! -nargs=0 -bar Qargs execute 'args ' . QuickfixFilenames()
function! QuickfixFilenames()
    " Building a hash ensures we get each buffer only once
    let buffer_numbers = {}
    for quickfix_item in getqflist()
        let buffer_numbers[quickfix_item['bufnr']] = bufname(quickfix_item['bufnr'])
    endfor
    return join(values(buffer_numbers))
endfunction


