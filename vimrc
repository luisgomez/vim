" All system-wide defaults are set in $VIMRUNTIME/debian.vim (usually just
" /usr/share/vim/vimcurrent/debian.vim) and sourced by the call to :runtime
" you can find below.  If you wish to change any of those settings, you should
" do it in this file (/etc/vim/vimrc), since debian.vim will be overwritten
" everytime an upgrade of the vim packages is performed.  It is recommended to
" make changes after sourcing debian.vim since it alters the value of the
" 'compatible' option.

" This line should not be removed as it ensures that various options are
" properly set to work with the Vim-related packages available in Debian.
runtime! debian.vim

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Operating System Specific Settings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
if has("mac") || has("macunix")
    set gfn=Bitstream\ Vera\ Sans\ Mono:h13
    set shell=/bin/bash
elseif has("win16") || has("win32") || has("win64")
    set gfn=Consolas:h10
    let g:tagbar_ctags_bin="~/.vim/exe/ctags.exe"
    let g:gutentags_ctags_executable="~/.vim/exe/ctags.exe"
    set ffs=dos,unix
    " Use ag.exe for FAST grep!
    let agprog='~/.vim/exe/ag.exe'
    let &grepprg = expand(agprog).' --nogroup --nocolor'
    let g:ctrlp_user_command = agprog.' %s -l --nocolor -g ""'
    let g:ctrlp_use_caching = 0
elseif has("linux")
    set gfn=Monospace\ 10
    set shell=/bin/bash
    if executable('ag') " use ag if available
        set grepprg=ag\ --nogroup\ --nocolor
    endif
endif

" ==================================================================
" pathogen - place as close to top as possible
" ==================================================================
runtime! bundle/pathogen/autoload/pathogen.vim
execute pathogen#infect()

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => General
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Sets how many lines of history VIM has to remember
set history=300
" Enable filetype plugin
filetype plugin on
filetype indent on
" Set to auto read when a file is changed from the outside
set autoread
" When vimrc is edited, reload it
autocmd! bufwritepost vimrc source ~/.vim/vimrc
" Turn on line numbers:
set number
" Toggle line numbers with F2 and fold column for easy copying:
nnoremap <F2> :set nonumber!<CR>:set foldcolumn=0<CR>
" Open vertical split windows on the right
set splitright
" load .vimrc from any directory it's found, but with limited permissions
set exrc
" status bar
set laststatus=2
set statusline=   " clear the statusline for when vimrc is reloaded
set statusline+=%-3.3n\                      " buffer number
set statusline+=%f\                          " file name
set statusline+=%{fugitive#statusline()}     " Git Hotness
set statusline+=%h%m%r%w                     " flags
set statusline+=[%{strlen(&ft)?&ft:'none'},  " filetype
set statusline+=%{strlen(&fenc)?&fenc:&enc}, " encoding
set statusline+=%{&fileformat}]              " file format
set statusline+=%=                           " right align
set statusline+=%{synIDattr(synID(line('.'),col('.'),1),'name')}\  " highlight
set statusline+=%b,0x%-8B\                   " current char
set statusline+=%-14.(%l,%c%V%)\ %<%P        " offset

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" <Leader> Mappings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let mapleader = ","
let g:mapleader = ","
" Fast saving
nmap <Leader>w :w!<cr>
" Fast quitting
nmap <Leader>q :q<cr>
" Fast editing of the .vimrc
map <Leader>e :e! ~/.vim/vimrc<cr>
" Clear all the search highlight terms
nnoremap <Leader>/ :noh<CR>
" Insert blank lines above, below without changing cursor (ranges allowed)
map <Leader>O :<C-U>call append(line(".") -1, repeat([''], v:count1))<CR>
map <Leader>o :<C-U>call append(line("."), repeat([''], v:count1))<CR>
" Move text from cursor onward to next line
nnoremap <Leader><CR> i<CR><Esc>l

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Diff options
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set diffopt=filler,vertical

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Colors and Fonts
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
syntax enable "Enable syntax hl

if has("gui_running")
    set guioptions-=T
endif

"LG make these setting default for console and gui, since
"   running under gnome-terminal supports all this junk
set t_Co=256
colorscheme PaperColor
set bg=dark

set encoding=utf8
try
    lang en_US
catch
endtry


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => VIM user interface
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" set cul        "Highlight the cursor line
" set cuc        "Highlight the cursor column
set wildmenu    "Turn on WiLd menu
set mouse=a     " Enable mouse usage (all modes) in terminals
set ruler       "Always show current position
set cmdheight=2 "The commandbar height
set hid         "Change buffer - without saving
" Set backspace config
set backspace=eol,start,indent
set whichwrap+=<,>,h,l
"set ignorecase "Ignore case when searching
set smartcase   "Smart case matching
set showcmd     " Show (partial) command in status line.
set hlsearch    "Highlight search things
set incsearch   "Make search act like search in modern browsers
set magic       "Set magic on, for regular expressions
set showmatch   "Show matching bracets when text indicator is over them
set mat=2       "How many tenths of a second to blink

" Disable annoying screen flash and/or bells when you press 'esc'
set noerrorbells visualbell t_vb=
autocmd GUIEnter * set visualbell t_vb=

" Have Vim jump to the last position when reopening a file
if has("autocmd")
  au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
    \| exe "normal! g'\"" | endif
endif



" ==================================================================
" UltiSnips Options
" ==================================================================
" Luis: Because the default mapping of <C-j> and <C-k> conflict with
" moving lines up/down, remap to tab/shift-tab
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"

" ==================================================================
" Enable moving lines up/down with CTRL-k/CTRL-j
" ==================================================================
function! MoveLineUp()
    call MoveLineOrVisualUp(".", "")
endfunction
function! MoveLineDown()
    call MoveLineOrVisualDown(".", "")
endfunction
function! MoveVisualUp()
    call MoveLineOrVisualUp("'<", "'<,'>")
    normal gv
endfunction
function! MoveVisualDown()
    call MoveLineOrVisualDown("'>", "'<,'>")
    normal gv
endfunction
function! MoveLineOrVisualUp(line_getter, range)
    let l_num = line(a:line_getter)
    if l_num - v:count1 - 1 < 0
        let move_arg = "0"
    else
        let move_arg = a:line_getter." -".(v:count1 + 1)
    endif
    call MoveLineOrVisualUpOrDown(a:range."move ".move_arg)
endfunction
function! MoveLineOrVisualDown(line_getter, range)
    let l_num = line(a:line_getter)
    if l_num + v:count1 > line("$")
        let move_arg = "$"
    else
        let move_arg = a:line_getter." +".v:count1
    endif
    call MoveLineOrVisualUpOrDown(a:range."move ".move_arg)
endfunction
function! MoveLineOrVisualUpOrDown(move_arg)
    let col_num = virtcol(".")
    execute "silent! ".a:move_arg
    execute "normal! ".col_num."|"
endfunction
nnoremap <silent> <C-k> :<C-u>call MoveLineUp()<CR>
nnoremap <silent> <C-j> :<C-u>call MoveLineDown()<CR>
inoremap <silent> <C-k> <C-o>:<C-u>call MoveLineUp()<CR>
inoremap <silent> <C-j> <C-o>:<C-u>call MoveLineDown()<CR>
vnoremap <silent> <C-k> :<C-u>call MoveVisualUp()<CR>
vnoremap <silent> <C-j> :<C-u>call MoveVisualDown()<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Text, tab and indent related
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set shiftwidth=4
set tabstop=4
set expandtab
set lbr
set tw=500
set ai "Auto indent
set nosmartindent " NO Smart indent
set wrap "Wrap lines

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Files and backups
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Turn backup off, since most stuff is in SVN, git anyway...
set nobackup
set nowb
set noswapfile

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Visual mode related
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" ,* searches for word under the cursor in files
nnoremap <Leader>* :grep! "<cword>"<CR>:cw<CR>

" When you press gv you vimgrep after the selected text
vnoremap <silent> gv :call VisualSearch('gv')<CR>
map <Leader>g :vimgrep // **/*.<left><left><left><left><left><left><left>

" In visual mode when you press * or #, search for the current selection
" From an idea by Michael Naumann
vnoremap <silent> * :call VisualSearch('f')<CR>
vnoremap <silent> # :call VisualSearch('b')<CR>
function! CmdLine(str)
    exe "menu Foo.Bar :" . a:str
    emenu Foo.Bar
    unmenu Foo
endfunction 
function! VisualSearch(direction) range
    let l:saved_reg = @"
    execute "normal! vgvy"
    let l:pattern = escape(@", '\\/.*$^~[]')
    let l:pattern = substitute(l:pattern, "\n$", "", "")
    if a:direction == 'b'
        execute "normal ?" . l:pattern . "^M"
    elseif a:direction == 'gv'
        call CmdLine("vimgrep " . '/'. l:pattern . '/' . ' **/*.')
    elseif a:direction == 'f'
        execute "normal /" . l:pattern . "^M"
    endif
    let @/ = l:pattern
    let @" = l:saved_reg
endfunction

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Quickfix window Options
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Always move the quickfix to bottom, full-width
autocmd! FileType qf wincmd J


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Parenthesis/bracket expanding
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Wrap stuff around visual text
" Typing a comma + the punctutation will wrap selection with punctuation
vnoremap <Leader>( <esc>`>a)<esc>`<i(<esc>
vnoremap <Leader>[ <esc>`>a]<esc>`<i[<esc>
vnoremap <Leader>{ <esc>`>a}<esc>`<i{<esc>
vnoremap <Leader>< <esc>`>a><esc>`<i<<esc>
vnoremap <Leader>' <esc>`>a'<esc>`<i'<esc>
vnoremap <Leader>" <esc>`>a"<esc>`<i"<esc>
vnoremap <Leader><space> <esc>`>a<space><esc>`<i<space><esc>
nnoremap <Leader><space> i<space><esc>la<space><esc>l
" This bad boy will let you wrap a visual selection with a {, but with a twist:
"   it will place a newline before both the opening and closing brace
"   all lines are also reindented to make them pretty
vnoremap <Leader><Return>{ <esc>`>a<cr>}<esc>`<i{<cr><esc>gvjj=

" ==================================================================
" TaskList Options
" ==================================================================
let g:tlWindowPosition = 1 "open on the bottom
map TL <Plug>TaskList

" ==================================================================
" Tagbar Commands/Variables
" ==================================================================
" Shorter commands to toggle Taglist display
nnoremap TT :TagbarToggle<CR>
let g:tagbar_compact=1 " save screen real estate
let g:tagbar_sort=1
" let g:tagbar_singleclick=1
" let g:tagbar_autoclose=1

" ==================================================================
" Custom NERDCommenter commands
" ==================================================================
let NERDSpaceDelims=1 " Add 1 space between each left/right comment delimiter
let NERDCommentWholeLinesInVMode=2 " whole lines commented out when there is no multipart delimiters but 
                                   " EXACT text that was selected is commented out if there IS multipart delimiters

" ==================================================================
" C/C++ Section
" ==================================================================
au BufRead,BufNewFile *.h,*.hpp,*.c,*.cpp,*.dox set ft=cpp.doxygen
set cinoptions=:0(0

" ==================================================================
" Python Section
" ==================================================================
au FileType python syn keyword pythonBuiltin self
autocmd FileType python set omnifunc=pythoncomplete#Complete
" Prevent from inserting first match
set completeopt=menuone,longest,preview
" Ctrl-Space launches omnicomplete
inoremap <Nul> <C-x><C-o>

" ==================================================================
" Save and restore folds on file open
" ==================================================================
"au BufWinLeave * mkview
"au BufWinEnter * silent loadview

" ==================================================================
" Strip trailing whitespace on save
" ==================================================================

fun! <SID>StripTrailingWhitespaces()
    let l = line(".")
    let c = col(".")
    %s/\s\+$//e
    call cursor(l, c)
endfun
" TODO: move these into file-specific vimscripts
autocmd FileType c,cpp,java,php,ruby,python autocmd BufWritePre <buffer> :call <SID>StripTrailingWhitespaces()

" ==================================================================
" Strip all '^M' characters (for files with mixed dos/unix line endings
" ==================================================================
command! StripCR %s/\%x0d$//g

" ==================================================================
" Toggle showing tabs
" ==================================================================
set list " show tab characters by default
nnoremap <Leader>l :set list!<CR>
set listchars=tab:»\ ,trail:·

" ==================================================================
" Index source code at current directory
" ==================================================================
fun! ExecCtagsRecursive()
    let l:commandline = ""
    if filereadable(".ctagsignore")
        let l:commandline .= ' --exclude=@.ctagsignore'
    endif
    if filereadable(".ctagsident")
        let l:commandline .= ' -I @.ctagsident'
    endif
    if filereadable(".ctagsextra")
        let l:commandline .= ' -L .ctagsextra'
    endif
    if !exists("g:tagbar_ctags_bin")
        execute '!ctags' . l:commandline . ' -R .'
    else
        execute '!' . expand(g:tagbar_ctags_bin) . l:commandline . ' --extra=+f -R .'
    endif
endfun
command! Index call ExecCtagsRecursive()

" ==================================================================
" screen_restore.vim settings
" ==================================================================
" To enable the saving and restoring of screen positions.
let g:screen_size_restore_pos = 1

" To save and restore screen for each Vim instance.
" This is useful if you routinely run more than one Vim instance.
" For all Vim to use the same settings, change this to 0.
let g:screen_size_by_vim_instance = 1

" ==================================================================
" fugitive/GV settings
" ==================================================================

cnoreabbrev GVA GV --all --remotes

" ==================================================================
" vim-easy-align settings
" ==================================================================

" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)

" " Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)

